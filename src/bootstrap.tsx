import ReactDOM from 'react-dom/client';
import Main from './Main';

const root = ReactDOM.createRoot(document.getElementById('note.client__root') as HTMLElement)
root.render(<Main />);