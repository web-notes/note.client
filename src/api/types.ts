type AuthParams = {
    name: string,
    password: string
}

type LoginAnswer = {
    accessToken: string,
    expirationDate: string
}

type Note = {
    id: string,
    title?: string,
    text: string,
    modified: string
}

type CreateNoteParams = {
    title: string,
    text: string,
}

type UpdateNoteParams = {
    id: string,
    title?: string,
    text: string,
}

type DeleteNoteParams = {
    id: string,
}

type ErrorType = {
    isVisible: boolean
    message: string
}