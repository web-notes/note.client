import axios  from "axios";

export const api = axios.create({
    baseURL: process.env.REACT_APP_BASE_API_URL,
});


api.interceptors.request.use(
    config => {
        config.headers!['Authorization'] = `Bearer ${localStorage.getItem("token")}`
            
        return config;
    }
)

export const methods = {
    login(data: AuthParams) {
        return api.post<LoginAnswer>("/user/login", data)
    },
    register(data: AuthParams) {
        return api.post<LoginAnswer>("/user/register", data)
    },

    getNotes() {
        return api.get<Note[]>("/Note")
    },
    createNote(data: CreateNoteParams) {
        return api.post<Note>("/Note", {...data})
    },
    deleteNote(data: DeleteNoteParams) {
        return api.delete("/Note", {data: data})
    },
    updateNote(data: UpdateNoteParams) {
        return api.put<Note>("/Note", {...data})
    },
}