import React, { useState, useMemo } from "react"
import { 
    Box, 
    Typography, 
    TextField, 
    Button,
    FormControl,
    InputLabel,
    OutlinedInput,
    InputAdornment,
    IconButton,
    Link
} from "@mui/material"
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { methods } from "../../api/methods"
import AlertComponent, { AlertTypes, AlertStateType } from "../../components/utilities/Alert"
import { AxiosError } from "axios";


export const firstRenderHandle = (setIsAuth) => {
    useMemo(() => {
        if(localStorage.getItem("token"))
            setIsAuth(true)
    }, [])
}


const Login: React.FC<{setIsAuth, isRegistered?: boolean}> = ({setIsAuth}) => {

    const [showPassword, setShowPassword] = useState<boolean>(false);
    const [isRegistered, setIsRegistered] = useState<boolean>(false);

    const [userData, setUserData] = useState<AuthParams>({
        password: "",
        name: ""
    })
    const [error, setError] = useState<AlertStateType>({
        isVisible: false,
        message: ""
    })

    const handleClick = async () => {
        try {
            if(isRegistered) await methods.register(userData)
            
            const {data} = await methods.login(userData)

            localStorage.setItem("token", data.accessToken)
            setIsAuth(true)
        }
        catch(e: AxiosError | any) {
            setError({message: e.response.data.errors[""][0] ?? e.response.data.title, isVisible: true})
            setIsAuth(false)
        }
    }

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
      event.preventDefault();
    };

    const handleRegisterClick = () => setIsRegistered(true)
    const handleLoginClick = () => setIsRegistered(false)

    return (
        <Box sx={{height: "100vh", display: "flex", alignItems: "center", justifyContent: "center"}}>
            <Box 
                display="flex" 
                flexDirection="column" 
                alignItems="center"
                gap="30px"
            >
                <Typography variant="h4" sx={{fontWeight: "400", textAlign: "center" }}>
                    {isRegistered ? "Sign up" : "Sign in"}
                </Typography>

                <TextField 
                    sx={{width: "300px"}}
                    error={error.isVisible}
                    onChange={(e) => setUserData({...userData, name: e.target.value})} 

                    label="Name" 
                    type="text"
                />

                <FormControl sx={{width: "300px"}} variant="outlined">
                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                    <OutlinedInput
                        error={error.isVisible}
                        type={showPassword ? 'text' : 'password'}
                        onChange={(e) => setUserData({...userData, password: e.target.value})} 
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                    edge="end"
                                >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                        label="Password"
                    />
                </FormControl>
                
                <Button variant='contained' sx={{width: "200px"}} onClick={handleClick}>{isRegistered ? "Register" : "Enter"}</Button>
                {!isRegistered 
                    ? <Typography>Don't have an account? <Link sx={{cursor: "pointer"}} onClick={handleRegisterClick}>Register</Link></Typography>

                    : <Typography>Already have an account? <Link sx={{cursor: "pointer"}} onClick={handleLoginClick}>Login</Link></Typography>
                }
                
            </Box>

            <AlertComponent 
                type={AlertTypes.error} 
                message={error.message} 
                isVisible={error.isVisible} 
                onCloseHandle={() => setError({message: "", isVisible: false})}
            />
        </Box>
    )
}

export default Login