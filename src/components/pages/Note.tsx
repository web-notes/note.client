import { Box, Typography, IconButton, useTheme } from "@mui/material"
import React, { FC, Fragment, useState } from "react"
import { methods } from "../../api/methods"
import DeleteIcon from '@mui/icons-material/Delete';
import { scrollTop } from "./Notes"
import AlertComponent, { AlertTypes } from "../../components/utilities/Alert"

const Note: FC<{
    item: Note
    index: number
    setNotesList: React.Dispatch<React.SetStateAction<Note[]>>
    setIsEditing: React.Dispatch<React.SetStateAction<boolean>>
    setEditingNote: React.Dispatch<React.SetStateAction<Note | null>>
    editingNote: Note | null
}> = ({item, index, setNotesList, setEditingNote, editingNote, setIsEditing}) => {

    const theme = useTheme()

    const [warning, setWarning] = useState<ErrorType>({
        isVisible: false,
        message: ""
    })

    const handleDeleteClick = async (noteId) => {
        await methods.deleteNote({id: noteId})

        setNotesList(prev => prev.filter(item => item.id !== noteId))
        setEditingNote(null)
        scrollTop()
        setIsEditing(false)
    }

    const openWarningAlert = (e) => {
        e.stopPropagation()
        e.preventDefault()

        setWarning({
            isVisible: true,
            message: "Are you sure you want to delete this note?"
        })
    }

    const handleEditClick = (item, index) => {
        setEditingNote(item)
        setIsEditing(true)
        scrollTop()
    }

    return (<Fragment>
        <Box onClick={() => handleEditClick(item, index)} key={item.id} sx={{
            bgcolor: editingNote?.id == item.id ? theme.palette.primary.main : "transparent", 
            maxWidth: "100%",
            cursor: "pointer",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            padding: "5px 30px",
            borderRadius: "5px",
            position: "relative",
            zIndex: 9,
            border: editingNote?.id == item.id ? "none" : "2px solid black"
        }}>
            <Typography sx={{color: editingNote?.id == item.id ? "white" : "black",}}>{item?.title}</Typography>
            <Box sx={{ position: "relative", zIndex: 10, display: "flex", justifyContent: "flex-end", gap: "10px"}}>
                <IconButton onClick={openWarningAlert}>
                    <DeleteIcon sx={{color: editingNote?.id == item.id ? "white" : "black" }} />
                </IconButton>
            </Box>
        </Box>

        <AlertComponent 
            type={AlertTypes.warning} 
            message="Are you sure you want to DELETE this note" 
            isVisible={warning.isVisible} 
            onCloseHandle={() => setWarning({message: "", isVisible: false})}
            buttonYesText="Yes"
            buttonNoText="No"
            onButtonYesClick={() => handleDeleteClick(item.id)}
            onButtonNoClick={() => setWarning({message: "", isVisible: false})}
        />

    </Fragment>)
}

export default Note