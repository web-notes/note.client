import { Box, Button, IconButton } from "@mui/material"
import AlertComponent, { AlertTypes } from "../../components/utilities/Alert"
import CustomLoader from "../../components/utilities/Loader"
import React, { useState, useEffect } from "react"
import { methods } from "../../api/methods"
import LogoutIcon from '@mui/icons-material/Logout';
import BigInput from "../../components/utilities/BigInput"
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import { AxiosError } from "axios"
import NotesList from "./NotesList"

export const scrollTop = () => window.scrollTo({
    top: 0,
    left: 0,
    behavior: "smooth"
})

const Notes = ({setIsAuth}) => {
    const isMobileScreen = window.innerWidth < 767
    
    const [error, setError] = useState<ErrorType>({
        isVisible: false,
        message: ""
    })
    const [isLoading, setIsLoading] = useState<boolean>(false)

    const [notesList, setNotesList] = useState<Note[]>([])

    const [isEditing, setIsEditing] = useState<boolean>(false)
    const [editingNote, setEditingNote] = useState<Note | null>(null)

    const fetchData = async () => {
        try {
            setIsLoading(true)

            const {data} = await methods.getNotes()

            setNotesList(data)
        }
        catch(e: AxiosError | any) {
            setError({message: e.response.data.errors[""][0] ?? e.response.data.title, isVisible: true})
        }
        finally {
            setIsLoading(false)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const exitHandle = () => {
        setIsAuth(false)
        localStorage.clear()
    }
    
    return (<Box sx={{padding: "50px 0 150px 0"}}>
        <Box sx={{
            display: "grid",
            gridTemplateColumns: "2.5fr 450px",
            gap: "80px",

            "@media screen and (max-width: 991px)": {
                display: "flex",
                flexDirection: "column"
            }
        }}>
            <BigInput 
                editingNote={editingNote} 
                setEditingNote={setEditingNote}
                isEdititng={isEditing} 
                setIsEditing={setIsEditing} 
                setError={setError} 
                setNotesList={setNotesList}
            />
            <NotesList 
                editingNote={editingNote} 
                setIsEditing={setIsEditing} 
                setEditingNote={setEditingNote} 
                notesList={notesList} 
                setNotesList={setNotesList}
            />
        </Box>

        <CustomLoader isLoading={isLoading}/>

        <AlertComponent 
            type={AlertTypes.error} 
            message={error.message} 
            isVisible={error.isVisible} 
            onCloseHandle={() => setError({message: "", isVisible: false})}
        />

        {isMobileScreen &&
            <ArrowUpwardIcon onClick={scrollTop} sx={{
                cursor: "pointer",
                position: "fixed",
                bottom: "30px",
                left: "30px",
                width: 35,
                height: 35,
            }} />
        }

        <Button 
            sx={{width: "max-content", position: "absolute", right: 30, bottom: 30}} 
            endIcon={<LogoutIcon />} 
            color="secondary" 
            variant='contained' 
            onClick={exitHandle}
        >
            Log out
        </Button>
    </Box>)
}

export default Notes