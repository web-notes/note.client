import { Box, Typography } from "@mui/material"
import React, { FC } from "react"
import Note from "./Note"

const NotesList: FC<{
    notesList: Note[]
    setNotesList: React.Dispatch<React.SetStateAction<Note[]>>
    setEditingNote: React.Dispatch<React.SetStateAction<Note | null>>
    setIsEditing: React.Dispatch<React.SetStateAction<boolean>>
    editingNote: Note | null
}> = ({notesList, setEditingNote, setIsEditing, setNotesList, editingNote}) => {

    return <Box>
        <Typography sx={{marginBottom: "30px", height: "36px"}} variant="h6">Your notes:</Typography>

        <Box sx={{
            maxHeight: "65vh", 
            overflowY: "auto", 
            display: "flex", 
            flexDirection: "column", 
            gap: "10px",

            "&::-webkit-scrollbar": {
                width: "18px",
                borderRadius: "5px"
            },
            
            "&::-webkit-scrollbar-track": {
                backgroundColor: "white",
            },
            
            "&::-webkit-scrollbar-thumb": {
                backgroundColor: "#000000",
                borderRadius: "10px",
                border: "7px solid white"
            }
        }}>
            {notesList.map((noteData, index) => 
                <Note 
                    key={noteData.id} 
                    item={noteData} 
                    index={index} 
                    setNotesList={setNotesList} 
                    setEditingNote={setEditingNote} 
                    editingNote={editingNote} 
                    setIsEditing={setIsEditing}
                />
            )}
        </Box>
    </Box>
}

export default NotesList