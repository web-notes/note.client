import { 
    Box, 
    TextField, 
    Button,
    useTheme
} from "@mui/material"
import { methods } from "../../api/methods"
import React, { useState, useEffect } from "react"
import { AxiosError } from "axios"
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const BigInput: React.FC<{
    setError: React.Dispatch<React.SetStateAction<ErrorType>>, 
    setNotesList: React.Dispatch<React.SetStateAction<Note[]>>, 
    isEdititng?: boolean, 
    setIsEditing: React.Dispatch<React.SetStateAction<boolean>>,
    setEditingNote: React.Dispatch<React.SetStateAction<Note | null>>
    editingNote: Note | null
}> = ({setError, setNotesList, isEdititng, setIsEditing, editingNote, setEditingNote}) => {
    const [title, setTitle] = useState<string>("")
    const [text, setText] = useState<string>("")

    useEffect(() => {
        setTitle(editingNote?.title ?? "")
        setText(editingNote?.text ?? "")
    }, [editingNote])

    const handleCreate = async () => {
        try {
            const {data} = await methods.createNote({
                title,
                text
            })

            setNotesList(prev => [...prev, data])

            handleCancel()
        }
        catch(e: AxiosError | any) {
            setError({message: e.response.data.errors[""][0] ?? e.response.data.title, isVisible: true})
        }
    }

    const handleEdit = async () => {
        try {
            const {data} = await methods.updateNote({
                title,
                text,
                id: editingNote!.id
            })

            setNotesList(prev => [...prev.map(item => {
                if(item.id !== editingNote?.id)
                    return item
                else return data
            })])
        }
        catch(e: AxiosError | any) {
            setError({message: e.response.data.errors[""][0] ?? e.response.data.title, isVisible: true})
        }
    }

    const handleCancel = () => {
        setIsEditing(false)
        setEditingNote(null)
        setTitle("")
        setText("")
    }

    const modules = {
        toolbar: [
            [{ size: [false, 'large', 'huge' ]}],
            ['bold', 'italic', 'underline','strike', 'blockquote'],
            [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
            ['link', 'image'],
            ['clean']
        ],
    }

    return (<Box component="form">
        <Box sx={{display: "flex", gap: "10px", marginBottom: "30px"}}>
            <Button 
                sx={{width: "150px"}} 
                variant='contained' 
                onClick={isEdititng ? handleEdit : handleCreate}
                color="primary"
            >
                {isEdititng ? "Save" : "Create"}
            </Button>

            {isEdititng && 
                <Button 
                    sx={{width: "150px"}} 
                    variant='contained' 
                    onClick={handleCancel}
                    color="secondary"
                >
                    Cancel
                </Button>
            }
        </Box>

        <TextField
            sx={{
                width: "100%", 
                borderRadius: "4px",
                marginBottom: "10px",

                ".MuiOutlinedInput-notchedOutline": {
                    border: "2px solid black",
                }
            }}
            onChange={(e) => setTitle(e.target.value)} 
            value={title}
            type="text"
            label={isEdititng ? 'New Title' : "Title"}
        />

        <ReactQuill modules={modules} style={{height: "500px", border: "none"}} theme="snow" value={text} onChange={setText} />
    </Box>)
}

export default BigInput