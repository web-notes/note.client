import { Alert, AlertTitle, Button, Typography, Box } from "@mui/material";

export enum AlertTypes {
    warning = "warning",
    error = "error",
    info = "info",
    success = "success"
}

export type AlertStateType = {
    isVisible: boolean
    message: string
}

export type ConfirmAlertType = {
    isVisible: boolean
    isConfirm: boolean
}

const AlertComponent: React.FC<{
    message: string, 
    isVisible: boolean, 
    onCloseHandle: () => void, 
    type: AlertTypes
    onButtonYesClick?: () => void
    onButtonNoClick?: () => void
    buttonYesText?: string
    buttonNoText?: string
}> = ({message, isVisible, onCloseHandle, type, buttonYesText, buttonNoText, onButtonYesClick, onButtonNoClick}) => {
    return (
        isVisible && <Alert sx={{position: "fixed", bottom: "15px", left: "15px", width: "calc(100% - 60px)", zIndex: 10}} onClose={onCloseHandle} severity={type}>
            <AlertTitle sx={{textTransform: "uppercase"}}>{type}</AlertTitle>
            <Typography>{message}</Typography>

            <Box display="flex" gap="20px" sx={{mt: 2}}>
                {buttonYesText && <Button sx={{width: "100px"}} color="error" variant="contained" onClick={onButtonYesClick}>{buttonYesText}</Button>}
                {buttonNoText && <Button sx={{width: "100px"}} variant="contained" onClick={onButtonNoClick}>{buttonNoText}</Button>}
            </Box>
        </Alert>
    )
}

export default AlertComponent