import { createContext, useState } from "react"
import { ThemeProvider, createTheme, CssBaseline, Container } from "@mui/material"
import Login, { firstRenderHandle } from "./components/pages/Login"
import Notes from "./components/pages/Notes"
import "./styles.scss"

export const UserContext = createContext<LoginAnswer | null>(null)

const Main = () => {
    const [isAuth, setIsAuth] = useState<boolean>(false)

    const theme = createTheme({
        palette: {
            primary: {
              main: '#161616',
              light: '#e8e8e8',
              dark: "#424242"
            },
            secondary: {
              main: '#7079d1',
              light: "#e8e8e8",
              dark: "#6169b9"
            },
        },
        typography: {
            fontFamily: [
                'Segoe UI',
                'Roboto',
                'Helvetica Neue',
                'Segoe UI Emoji',
                'Segoe UI Symbol',
                'BlinkMacSystemFont',
            ].join(','),
        },
    })

    firstRenderHandle(setIsAuth)

    return (
        <ThemeProvider theme={theme}>
            <Container maxWidth={false} id="backoffice__container">
                {isAuth ? <Notes setIsAuth={setIsAuth} /> : <Login setIsAuth={setIsAuth} />}
            </Container>
        </ThemeProvider>
    )
}

export default Main