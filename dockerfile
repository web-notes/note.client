FROM node:19.6.0-alpine as build

ARG BASE_URL
ENV REACT_APP_BASE_API_URL=$BASE_URL

WORKDIR /usr/app
COPY . /usr/app
RUN npm i
RUN npm run build

FROM nginx:1.23.1-alpine
EXPOSE 80

COPY --from=build /usr/app/public /usr/share/nginx/html